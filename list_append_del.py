sample_list = ["hari","shyam","santosh","pujan"]
print(sample_list)
del sample_list[2]
print(sample_list)
sample_list.append("gopal")
print(sample_list)
sample_list[::-1]
print(sample_list)
sample_list[0]="gopal"
print(sample_list)
sample_list[1]="tiwari"
print(sample_list)
print(sample_list.count("gopal"))
sample_list.insert(3,'Gourav')
print(sample_list)
sample_list.__delitem__(2)
print(sample_list)
print(sample_list.index("gopal"))
print(sample_list.index("Gourav"))
last_name=sample_list.pop()
print(last_name)
print(sample_list)
first_name=sample_list.pop(0)
print(sample_list)
print(first_name)

