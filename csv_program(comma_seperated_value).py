import csv

# with open("python_project.csp.csv","r", newline="\n") as read_file:
#     reader=csv.DictReader(read_file)
#     for row in reader:
#         print(row)
print("1...Only name")
with open("python_project.csp.csv", "r", newline="\n") as read_file:
    reader = csv.DictReader(read_file)
    for row in reader:
        print(row["Name"])
        # print(row["Gender"])
        # print(row["Address"])
print("\n"*2)
print("2...Male")
# printing only the male from the csv file
with open("python_project.csp.csv", "r", newline="\n") as read_file:
    reader = csv.DictReader(read_file)
    male_only=filter(lambda row: row["Gender"]=="m",reader)
    for male in male_only:
        print(male)
print("\n"*2)
print("3...Female")
# priting only the female for the csv file
with open("python_project.csp.csv", "r", newline="\n") as read_file:
    reader = csv.DictReader(read_file)
    female_only=filter(lambda  row: row['Gender']=="f",reader)

    for female in female_only:
        print(female)

print("n"*2)
print("4...")
with open("python_project.csp.csv", "r", newline="\n") as read_file:
    reader = csv.DictReader(read_file)
    sur_name_only=filter(lambda row:(row["Name"].split(" ")),reader )
    for sur_name in sur_name_only:
        print(sur_name)