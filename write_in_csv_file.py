import csv

#
# with open("python_project.csp.csv","r",newline="\n") as read_file:
#     reader=csv.DictReader(read_file)
#     males=filter(lambda row:row["Gender"]=="m",reader)

# printing all the male students for the dictionary
# with open("only_males.csv","w") as write_file:
#     writer=csv.DictWriter(write_file,fieldnames=['S.N','Name','Gender','Age','Mail Id','Phone Number','Address'])
#     writer.writeheader()
#     writer.writerows(males)


with open("python_project.csp.csv", "r", newline="\n") as read_file:
    reader = csv.DictReader(read_file)
    age = filter(lambda row: row["Age"]<"20", reader)
    with open("only_age_bleow_20.csv", "w") as write_file:
        writer = csv.DictWriter(write_file,fieldnames=['S.N', 'Name', 'Gender', 'Age', 'Mail Id', 'Phone Number', 'Address','Dob'])
        writer.writeheader()
        writer.writerows(age)

#printing the all the female student in the dictionary
# with open("python_project.csp.csv","r",newline="\n") as read_file:
#     reader=csv.DictReader(read_file)
#     females=filter(lambda row:row["Gender"]=="f",reader)
#     with open("only_females.csv","w") as write_file:
#         writer=csv.DictWriter(write_file,fieldnames=['S.N','Name','Gender','Age','Mail Id','Phone Number','Address','Dob'])
#         writer.writeheader()
#         writer.writerows(females)



# print("age below 20 using date of birth ")

def age_below_20(row):
    parts=row["Dob"].split("/")
    age=2074-int(parts[0])
    return age<30

with open("python_project.csp.csv","r",newline="\n") as read_file:
    reader=csv.DictReader(read_file)
    # for row in reader:
    #     age=2074-int(row["Dob"].split("/")[0])
    #     print(age)
    only_age_below_20=filter(age_below_20,reader)
    print(list(only_age_below_20))



