# a=[1,2,34,4]
# b=a
# b[2]="98"
# b.append(45)
# print(a)

# alising is generally used to optimization of the memory
# generally in the real life we can use the nick name of the variable which can be used in short form of the variable
# eg:-customer_request_pending to crp or cr  
a=[2,3,4,5,45]
b=a.copy()
b.append(32)
print(a)
print(b)