# In tupple we cannot change the value. We cannto append the data in the tupple
# tupple is used in database (which is used to minimum utilization of the memory)(in the efficient way)
# tupple used fixed memory location where as list is the dynamic memory location
# good in perfromance because of the fixed memory location is used
c = (4, 5, 6, 7)
d = (8, 9, 2, 4)
e = c + d
f = c * 2
slicing = c[::-1]
print("c=", c)
print("d=", d)
print("e=", e)
print("f=", f)
print("slicing of the element=", slicing)

# Example
# Example
# Example
print("\n" * 2)


def my_sum(a, b):
    return a + b


print("SUm of the two digits is =", my_sum(4, 5))

# we identify the problem is we can have the more than two data
# we can used as the same way up to certain level what when we have 1000 of data
print("\n" * 2)


def my_sum(*args):
    # here args is the variable we can use any of the variable
    print(args)
    print(type(args))


my_sum(4, 3, 2, 6)
print("\n" * 2)


# loop in the tupple

def my_sum(*args):
    total = 0
    for number in args:
        total += number
    return total


print(my_sum(4, 5, 6))

print("\n" * 2)


def another_function(*args, **kwargs):
    # kwargs = keyword arguments
    print(args)
    print(kwargs)


another_function(1, 2, 3, "gopal", name="Tiwari", rollno=45)
another_function()
# both the list and dictionary is empty
# here paramerter are optional


print("\n" * 2)

# aliasing means the fale name (nick name)
a = [3, 4, 5, 6, 7]
b = a
b.append(8)
print(a)
print(b)

print("\n" * 2)
a = [3, 4, 5, 6, 7]
b = a
c = [3, 4, 5, 6, 7]

# to identify the two value are alis or not

print(b == c)
print(a == c)
print(a is b)
# gives true because a and b are same
print(b is c)
print(a is c)
print("\n" * 2)



# to avoid this
a = [3, 4, 5, 6, 7]
b=a.copy()
# a is the person and b is the nick name of the a (here if we make changes in the a doesnt change in the a)
print(a is b)
# gives false