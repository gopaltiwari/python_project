# sample_list = [2, 4, 5, 8, 9, 10, 23, 45, 76, 81]
#
# # always boolean function is used by the filter
# # if true than append in the list
# # else discard the value
# divisible_by_five = lambda n: n % 5 == 0
# multiple_of_five = filter(divisible_by_five, sample_list)
# print(list(multiple_of_five))

#
# student_list=["gopal tiwari","binod tiwari","santosh khananl","pujan gautam","mukesh maharjan","gourav kadariya","umesh bhatta"]
# input_name=input("enter the name you want")
# sur_name_function=lambda name: name==input_name
# names_of_the_student=filter(sur_name_function,student_list)
# print(list(names_of_the_student))
# #
# student_list=["gopal tiwari","binod kumar tiwari","santosh raj khananl","pujan gautam","mukesh maharjan","gourav kadariya","umesh bhatta"]
# name_without_middle_name=filter(lambda name: len(name.split(" "))==3,student_list)
# print(list(name_without_middle_name))


# perfect_sqaure
import math

# number_list = [5, 10, 16, 75, 81, 4, 25]
# sqaureroot_of_number = lambda n: n % 3 == 0
# multiple_of_three = filter(sqaureroot_of_number, number_list)
# print(list(multiple_of_three))
#
# city_list = ["kathmandu,np", "walling,np", "goa,in", "new york,usa", "texas,usa", "pokhara,np"]
# places=filter(lambda place:place.split(",")[-1]=="np",city_list)
#
# # print(list(places))
#
#
#
# names_of_the_student=["Gopalramtiwari","pujankumar thapa","santoshbhadhurkhanal","shreekrishnakhanal","narayan tiwari","binod tiwari"]
#
# length_of_the_student= lambda name: len(name)>15
# filtering_name=filter(length_of_the_student,names_of_the_student)
# print(list(filtering_name))

#
# city_list = ["kathmandu,np", "walling,np", "goa,in", "new york,usa", "texas,usa", "pokhara,np"]
# for name in city_list:
#     parts=name.split(" ")
#     print(list(parts))

names_of_the_student=["Gopalramtiwari","pujankumar thapa","santoshbhadhurkhanal","shreekrishnakhanal","narayan tiwari","binod tiwari"]

names_with_a_at_starting=lambda name: name[0]=="n"

filtering_function=filter(names_with_a_at_starting,names_of_the_student)
print(list(filtering_function))



list_number=[1,2,3,34,0,-4,-2,56,0]
checking_function=lambda number: number<0
filtering_function=filter(checking_function,list_number)
print(list(filtering_function))


# only perfect square
import math
only_perfect_square=filter(lambda n:math.sqrt(n)==int(math.sqrt(n)),[2,3,4,5,0,9,16,81])
print(list(only_perfect_square))