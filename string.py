print("ram")
print('gopa')
# for long line triple cote is uses .....used to make more than 1 lines
# print('''ram''')
print('''My name is Gopal Tiwari...
I am a studet
''')
print('My name is gopal'
      'and I am a student')

# a[1:3 and 3:4]
# 'e'
# a[1:3 and 3:5]
# 'e'
# a[3:5 and 3:5]
# ''
# a[-5:-1]
# 'Hell'
# a[-3:-1]
# 'll'
# a[0:4]
# 'Hell'
# a[:4]
# 'Hell'
# a[:4]
# 'Hell'
# a[:4]
# 'Hell'
# a[1:4]
# 'ell'
# a[:2]
# 'He'
# a[2:]
# 'llo'
# a[3:]
# 'lo'
# a[:5]
# 'Hello'
# a[:2]
# 'He'
# a[1:2]
# 'e'
# a[0:2]
# 'He'
# a[-1:]
# 'o'
# a[:-1]
# 'Hell'
# b="gopal tiwari"
# b[0:5]
# 'gopal'
# b[0:6]
# 'gopal '
# b[0:8]
# 'gopal ti'
# b[-5:-1]
# 'iwar'
#
# b[2:4:2]
# 'p'
# b[::2]
# 'gpltwr'
# b[::1]
# 'gopal tiwari'
# b[:3:2]
# 'gp'
# b[:8:2]
# 'gplt'
# b[:11:6]
# 'gt'
# b[:11:8]
# # 'gw'
# name="My name is gopal  tiwari"
# name[::-2]
# 'iai lpgs mny'
# name[::-1]
# 'irawit  lapog si eman yM'
# name[:8:6]
# 'Me'
