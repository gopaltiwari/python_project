for i in range(1, 6):
    print("*" * i)

print("\n" * 2)

for i in range(5, 0, -1):
    print("*" * i)

print("\n" * 2)

for i in range(1, 6):
    print(" " * (5 - i) + "*" * i)

# for i in range(1,5):
#     print(i)

for i in range(5, 0, -1):
    print(" " * (5 - i) + "*" * i)

max_number_of_star = 5
space_count = max_number_of_star // 2
star_count = 1
for i in range(max_number_of_star):
    print("." * space_count + "*" * star_count)
    if i < max_number_of_star // 2:
        space_count -= 1
        star_count += 2
    else:
        space_count += 1
        star_count -= 2

max_number_of_star = 5
space_count = max_number_of_star // 2
star_count = 1
for i in range(max_number_of_star):
    if star_count - 2 > 0:
        print(" " * space_count + "*" + " " * (star_count - 2) + "*")
    else:
        print(" " * space_count + "*")
    if i < max_number_of_star // 2:
        space_count -= 1
        star_count += 2
    else:
        space_count += 1
        star_count -= 2

print(" \n" * 2)
# next approach for blank diamond
max_number_of_row = 5
number_of_row = 5
outer_space = max_number_of_row // 2
inner_space = -1
for i in range(number_of_row):
    if inner_space == -1:
        print("-" * outer_space + "*")
    else:
        print("-" * outer_space + "*" + "-" * inner_space + "*")

    if i < number_of_row // 2:
        outer_space -= 1
        inner_space += 2
    else:
        outer_space += 1
        inner_space -= 2





print("\n"*2)
max_number_of_row = 5
inner_space = -1
outerspace = max_number_of_row // 2
for i in range(max_number_of_row):
    if inner_space == -1:
        print("-" * outer_space + "*")
    else:
        print("-" * outer_space + "*" + "*" * inner_space)
    if i < number_of_row // 2:
        outer_space -= 1
        inner_space += 2
    elif i >number_of_row//2:
        outer_space += 1
        inner_space -= 2
    else:
        outer_space-=1
        inner_space+=1