# we can read write and append in the file
# we can save the file and can be used in the futher use
sample_file = open("hello.txt", "w")
sample_file.write("Hello world, It will be file and can be used in the futher used")
sample_file.close()

# another way to write the file
# with the "with" we have to used in space in one tab
# upper three line and below 2 line are equivalent
# pointer is set at the sample_file and we open in the write mode and something that can written in the file

with open("next_hello", "w")as sample_file_1:
    sample_file_1.write("hello world ,It will be used in the futher processing")

print("Hello")
# we have to close in the upper file but we should not in the second method
# generally we use second approach (because it is efficent then the upper approach)



with open("python_project.csp.csv","r") as student_file:
    for line in student_file:
        print(line)



# each line is print as one by one not the whole file
# it is used to manage the memory
# if whole file is open the it is not possible that we have small amout of the memory
# HD5 research yourself
