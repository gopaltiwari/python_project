# in operator is used to check whether the data is in the list or not
# in other word we can say that "in " operator is used to search the element in the list

5 in [2, 3, 4, 5]
True
6 in [4, 3, 2, 1]
False
